var express = require("express");
var router = express.Router();
const axios = require("axios");
const FormData = require("form-data");

const fs = require("fs");
const formidable = require('express-formidable')
router.use(formidable())    


const slackToken = "xoxb-3622860302448-3802823723056-YIs9jVEe0xQhgrMZLVfvPYzU";
// const slackToken = "xoxe.xoxp-1-Mi0yLTM2MjI4NjAzMDI0NDgtMzU4NDUwNTc5MzE5MS0zNzcxMDY0MTYzNjE5LTM3NjgyMTAyNTU3ODEtM2RmZDFlMDM0M2VjN2E4NzQ4MDZiYzY2NzkzYjlkNzNmYzZiMjkwNDBkMzliMjNhMDc4Mjk0YjA1ZmZkNTAwOQ";



router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});


router.post("/uploadFiles", function (req, res) {
    try {
      let run = async () => {
        const url = "https://slack.com/api/files.upload";
        const form = new FormData();
        form.append('file', fs.createReadStream(req.files.file.path));
        form.append('channels', req.fields.channel_id);
        return await axios.post(
          url,
          form,
          { headers: {
            'Authorization': `Bearer ${slackToken}`,
            'Content-Type': 'multipart/form-data'
          } }
        );
      };
  
      run().then((response) => {
        console.log("response", response);
        res.json({
          'response': response.data
        });
      }).catch((err) => {
        console.log(err);
      });
      
    } catch (error) {
      (err) => console.log(err);
    }
  });


module.exports = router;
